#include "FS.h"
#include "SD.h"
#include "SPI.h"
#include <LiquidCrystal.h>


/* GENERAL SETTINGS */
/********************/
#define delay_time 1000
#define samplingrate 30             // Number of samples for analog read
#define voltage 3.3

/* LED */
/*******/
#define led_pin 14

/* TEMPERATURE SENSOR */
/**********************/
#define ThermistorPin 4             // Pin, to which the voltage divider is connected
#define adcMax 4095.0               // Analog read max value
#define nominal_resistance 9700.0   // Nominal resistance at nominal temperature
#define nominal_temperature 288.15  // temperature for nominal resistance in Kelvin
#define beta 3950.0                 // The beta coefficient or the B value of the thermistor (usually 3000-4000) check the datasheet for the accurate value.
#define Rref 10000.0                // Value of  resistor used for the voltage divider
#define Vs 3.3                      // Voltage to sensor

/* LIGHT SENSOR */
/****************/
#define LightSensorPin 0
#define light_beta -0.8495
#define light_alpha 250.0
#define light_resistor 1000.0


/* DISPLAY */
/***********/
#define TFT_D7 15
#define TFT_D6 16
#define TFT_D5 17
#define TFT_D4 21
#define TFT_E 22
#define TFT_RS 2



/* SD CARD READER */
/******************/
#define SD_miso 19
#define SD_sck 18
#define SD_mosi 23
#define SD_cs 5

LiquidCrystal lcd(TFT_RS, TFT_E, TFT_D4, TFT_D5, TFT_D6, TFT_D7);

//Breeding Leds
#define Breeding_led 32

//pomp
#define pomp 35

void setup() {
  Serial.begin(115200);
  
  // LCD DISPLAY //
  // Set up the LCD's numvers of columns and rows
  lcd.begin(16, 2);
  // Print a message to the LCD
  lcd.setCursor(0, 0);
  lcd.print("Starting ...");

  // SETUP PINS //
  pinMode(led_pin, OUTPUT);
  pinMode(Breeding_led, OUTPUT);
  pinMode(pomp, OUTPUT);

  // SETUP SD-CARD //
  StartSDcard();
  appendFile(SD, "/data.txt", "temp, light intensiteit, time in millis\n");
}

void loop() {
  digitalWrite(pomp, HIGH);
  digitalWrite(Breeding_led, HIGH);
  digitalWrite(led_pin, HIGH);
  delay(1000);
  
  appendFile(SD, "/data.txt", String(ReadTemperatureSensor())+ String(", ") + String(ReadLightSensor()) + ", " + String(millis()) + String("\n"));
  
  lcd.setCursor(0, 0);
  lcd.print("temp:" + String(ReadTemperatureSensor()));
  lcd.setCursor(0, 1);
  lcd.print("light: " + String(ReadLightSensor()));

  delay(1000);
  digitalWrite(led_pin, LOW);
  
  delay(delay_time);
}

void appendFile(fs::FS &fs, const char * path, String message){
    File file = fs.open(path, FILE_APPEND);
    if(!file){
        Serial.println("Failed to open file for appending");
        return;
    }
    if(file.print(message)){
        Serial.println("Appended to " + String(path) + " the following message " + message);
    } else {
        Serial.println("Append failed");
    }
    file.close();
}

void failure(String message) {
  while (true) {
    Serial.println("Failure due to " + message);
    delay(delay_time);
  };
}

void StartSDcard() {
  if(!SD.begin(SD_cs)){
        Serial.println("Card Mount Failed");
        return;
    }
    uint8_t cardType = SD.cardType();

    if(cardType == CARD_NONE){
        Serial.println("No SD card attached");
        return;
    }

    uint64_t cardSize = SD.cardSize() / (1024 * 1024);
    Serial.printf("SD Card Size: %lluMB\n", cardSize);
}

int AnalogRead(uint8_t ntc_pin) {
  int val = 0;
  for (int i = 0; i < samplingrate; i++) {
    val += analogRead(ntc_pin);
    delay(1);
  }

  val = val / samplingrate;
  return val;
}

double CalculateResistance(uint8_t pin, double resistor) {
  double adc = AnalogRead(pin);
  double Vout = (adc * Vs)/adcMax;
  return (resistor * Vout) / (Vs - Vout); 
}

double ReadTemperatureSensor() {
  double Rt = CalculateResistance(ThermistorPin, Rref);
  return 1/(1/nominal_temperature + log(Rt/nominal_resistance)/beta) - 273.15;  // Calculate temperature
}

long ReadLightSensor() {
  double resistance = CalculateResistance(LightSensorPin, light_resistor);		  // Read light sensor output
  return pow((resistance/light_alpha), (1/light_beta));                         // Calculate the light intensity
}
