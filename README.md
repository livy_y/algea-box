# Algea Box
This is a STEM project where we monitor the growth of algea by placing them in a closed container. This closed container has a temperature sensor, light sensor and a light. When we want to measure the algea concentration we shine light trough the algea's and determin the concentration based on the light getting throug. We log this data on an SD-card for further proccessing.

# Getting started
TODO image electrical connection

After wiring the esp32 with the components you will need to flash the code with the arduino cli. But before flashing the code you will need to install the esp32 drive. To do this check out [this page](https://randomnerdtutorials.com/installing-the-esp32-board-in-arduino-ide-windows-instructions/) and follow the instructions to add the board library. Then install all libraries and flash the board.

